package com.mimaj.java.knowledge.base.algorithm.groundhog.api;

import java.util.List;

final class AvgOfNum {

    double streamSolution(final List<Integer> givenList) {
        return givenList.stream().mapToDouble(Integer::doubleValue).average().orElse(0.0);
    }
}
