package com.mimaj.java.knowledge.base.algorithm.groundhog.api;

final class HexToDec {

    String convert(final String input) {
        return String.valueOf(Long.parseLong(input, 16));
    }
}
