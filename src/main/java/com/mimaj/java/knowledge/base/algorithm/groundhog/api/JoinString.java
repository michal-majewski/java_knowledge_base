package com.mimaj.java.knowledge.base.algorithm.groundhog.api;

import java.util.List;

final class JoinString {

    String joinAllUsingStringApi(final List<String> strings) {
        return String.join("", strings);
    }
}
