package com.mimaj.java.knowledge.base.algorithm.groundhog.api;

import java.util.Arrays;

final class MaxNumber {

    int findUsingStreams(final int[] numbers) {
        return Arrays.stream(numbers).max().orElse(0);
    }
}
