package com.mimaj.java.knowledge.base.algorithm.groundhog.api;

final class ReverseString {

    String perform(final String given) {
        return new StringBuilder(given).reverse().toString();
    }
}
