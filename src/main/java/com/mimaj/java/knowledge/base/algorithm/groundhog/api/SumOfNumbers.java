package com.mimaj.java.knowledge.base.algorithm.groundhog.api;

import java.util.Arrays;

final class SumOfNumbers {

    int find(final int[] numbers) {
        return Arrays.stream(numbers).sum();
    }
}
