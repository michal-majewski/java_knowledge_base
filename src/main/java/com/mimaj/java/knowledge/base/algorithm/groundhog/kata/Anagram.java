package com.mimaj.java.knowledge.base.algorithm.groundhog.kata;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

final class Anagram {

    boolean check(final String firstWord, final String secondWord) {
        if (firstWord.length() != secondWord.length()) {
            return false;
        }
        final List<String> strings = Arrays.stream(secondWord.split("")).collect(Collectors.toList());
        Arrays.stream(firstWord.split("")).forEach(strings::remove);
        return strings.size() == 0;
    }
}
