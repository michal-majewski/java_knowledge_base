package com.mimaj.java.knowledge.base.algorithm.groundhog.kata;

import java.util.Arrays;

final class Count3 {

    int imperativeCheck(final String number) {
        var num = Integer.parseInt(number);
        var result = 0;

        while (num > 0) {
            if (num % 10 == 3) {
                result++;
            }
            num /= 10;
        }

        return result;
    }

    int streamCheck(final String number) {
        return (int) Arrays.stream(number.split("")).mapToInt(Integer::parseInt).filter(i -> i == 3).count();
    }
}
