package com.mimaj.java.knowledge.base.algorithm.groundhog.kata;

import java.util.Arrays;

final class DuplicatedNumbers {

    int count(final String[] numberArr) {
        final var numberList = Arrays.stream(numberArr).map(Integer::valueOf).sorted().toList();

        for (int i = 0; i < numberList.size() - 1;i++) {
            if (numberList.get(i).equals(numberList.get(i+1))) return numberList.get(i);
        }

        return 0;
    }
}
