package com.mimaj.java.knowledge.base.algorithm.groundhog.kata;

final class Fibonacci {

    long calcBasicRecursive(final int fiboNum) {
        if (fiboNum == 0) return 0;
        if (fiboNum == 1) return 1;

        return calcBasicRecursive(fiboNum - 1) + calcBasicRecursive(fiboNum - 2);
    }

    long calcEnhancedRecursive(final int fiboNum) {
        if (fiboNum == 0) return 0;
        if (fiboNum == 1) return 1;

        return calcEnhancedRecursive(0, 1, 2, fiboNum);
    }

    private long calcEnhancedRecursive(final long secondValue, final long firstValue, final int counter, final int fiboNum) {
        if (counter == fiboNum) {
            return firstValue + secondValue;
        }

        return calcEnhancedRecursive(firstValue, firstValue + secondValue, counter + 1, fiboNum);
    }
}
