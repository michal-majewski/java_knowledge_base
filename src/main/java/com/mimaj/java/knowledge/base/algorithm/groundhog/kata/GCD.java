package com.mimaj.java.knowledge.base.algorithm.groundhog.kata;

final class GCD {

    int find(int first, int second) {
        if (first % second == 0) {
            return second;
        }

        return find(second, first % second);
    }
}
