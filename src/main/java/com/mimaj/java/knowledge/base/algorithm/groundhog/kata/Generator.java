package com.mimaj.java.knowledge.base.algorithm.groundhog.kata;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

final class Generator {

    //FIXME: Check tests for the last two cases
    int[] findGenerators(final int prime) {
        final var generators = new ArrayList<Integer>();

        final int lowerBound = 1;
        final int upperBound = prime - 1;

        for (int possibleGenerator = lowerBound; possibleGenerator <= upperBound; possibleGenerator++) {
            if (isProduceUniques(possibleGenerator, prime)
                    && isReturnOneForUpperBound(possibleGenerator, prime)) {
                addGenerator(generators, possibleGenerator);
            }
        }

        return prepareResult(generators);
    }

    private boolean isProduceUniques(final int checkedNumb, final int primeNUmber) {
        final var numbSet = new HashSet<Integer>();

        for (int j = 1; j < primeNUmber; j++) {
            final BigInteger pow = BigInteger.valueOf((long) Math.pow(checkedNumb, j));

            final double v = pow.longValueExact() % primeNUmber;
            if (numbSet.contains((int) v)) {
                return false;
            }
            numbSet.add((int) v);
        }
        return true;
    }

    private boolean isReturnOneForUpperBound(final int i, final int upperBound) {
        return Math.pow(i, upperBound - 1) % upperBound == 1;
    }

    private void addGenerator(final ArrayList<Integer> generators, final int possibleGenerator) {
        generators.add(possibleGenerator);
    }

    private int[] prepareResult(final ArrayList<Integer> generators) {
        final int[] ints = generators.stream().mapToInt(Integer::intValue).toArray();
        System.out.println(Arrays.stream(ints).boxed().toList());
        return ints;
    }
}
