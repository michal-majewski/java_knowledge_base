package com.mimaj.java.knowledge.base.algorithm.groundhog.kata;

import java.util.Arrays;

final class IndexOf3 {

    int find(final int[] numbers) {
        return Arrays.stream(numbers).boxed().toList().indexOf(3);
    }
}
