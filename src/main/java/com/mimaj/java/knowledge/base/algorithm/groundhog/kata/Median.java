package com.mimaj.java.knowledge.base.algorithm.groundhog.kata;

import java.util.Arrays;

final class Median {

    float calc(final int[] numbers) {
        final int[] numbs = Arrays.stream(numbers).sorted().toArray();

        if (isLengthOddNumber(numbs)) {
            final int middleNumber = numbs.length / 2;
            return numbs[middleNumber];
        }

        final int centerRight = numbs.length / 2;
        final int centerLeft = (numbs.length / 2) - 1;
        return (float) ( numbs[centerRight] + numbs[centerLeft]) / 2;
    }

    private boolean isLengthOddNumber(final int[] numbs) {
        return numbs.length % 2 != 0;
    }

    float calcRecursive(final int[] numbers) {
        final int[] sortedNumbs = Arrays.stream(numbers).sorted().toArray();
        return recursiveMedian(sortedNumbs);
    }

    private float recursiveMedian(final int[] numbers) {
        if (numbers.length == 1) {
            return numbers[0];
        }

        if (numbers.length == 2) {
            return (float) (numbers[0] + numbers[1]) / 2;
        }

        return calcRecursive(Arrays.copyOfRange(numbers, 1, numbers.length - 1));
    }
}
