package com.mimaj.java.knowledge.base.algorithm.groundhog.kata;

final class Palindrome {

    boolean check(final String word) {
        if (word.length() <= 1) {
            return true;
        }

        if (word.charAt(0) == word.charAt(word.length() - 1)) {
            return check(word.substring(1,word.length() - 1));
        }

        return false;
    }
}
