package com.mimaj.java.knowledge.base.algorithm.groundhog.kata;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

final class RotateArray {

    int[] oneLeft(final int[] array) {
        final List<Integer> rotated = Arrays.stream(array).skip(1).boxed().collect(Collectors.toList());
        rotated.add(array[0]);
        return rotated.stream().mapToInt(Integer::intValue).toArray();
    }
}
