package com.mimaj.java.knowledge.base.algorithm.groundhog.kata;

import java.util.Arrays;

final class SecondMax {

    int find(final int[] numbers) {
        final var numbs = Arrays.stream(numbers).sorted().distinct().toArray();
        final int secondMax = numbs.length - 2;
        final int neutralValue = 0;
        return numbs.length > 1 ? numbs[secondMax] : neutralValue;
    }
}
