package com.mimaj.java.knowledge.base.algorithm.groundhog.kata;

import java.util.Arrays;

final class SumOfEvenNumbers {

    int calc(final int[] numbers) {
        return Arrays.stream(numbers).filter(value -> value % 2 == 0).sum();
    }
}
