package com.mimaj.java.knowledge.base.algorithm.regex;

import java.util.Arrays;

final class CountNumbers {

    int countInString(final String txt) {
        return (int) Arrays.stream(txt.split("\\W")).filter(s -> s.matches("\\d+")).count();
    }
}
