package com.mimaj.java.knowledge.base.algorithm.regex;

final class TimeValidation {

    private static final String HOUR_REGEX = "([0-1][0-9]|[2][0-3])";
    private static final String SEPARATOR = ":";
    private static final String MINUTES_AND_SECONDS_REGEX = "([0-5][0-9])";

    boolean isValid(final String time) {
        return time.matches(HOUR_REGEX + SEPARATOR + MINUTES_AND_SECONDS_REGEX + SEPARATOR + MINUTES_AND_SECONDS_REGEX);
    }
}
