package com.mimaj.java.knowledge.base.algorithm.task;

import java.util.List;

class CountElementsInList {

    <E> int recursive(List<E> elements) {
        if (elements.isEmpty()) {
            return 0;
        }
        elements.remove(0);
        return 1 + recursive(elements);
    }
}
