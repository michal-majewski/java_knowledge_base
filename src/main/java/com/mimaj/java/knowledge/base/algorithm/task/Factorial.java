package com.mimaj.java.knowledge.base.algorithm.task;

import java.util.stream.IntStream;

class Factorial {

    int recursive(int factorialNum) {
        return factorialNum == 0 || factorialNum == 1 ? 1 :
            factorialNum * recursive(factorialNum - 1);
    }

    int imperative(int factorialNum) {
        var result = 1;

        if (factorialNum == 0 || factorialNum == 1) {
            return result;
        }

        for (int i = factorialNum; i > 1; i--) {
            result *= i;
        }
        return result;
    }

    int stream(int factorialNum) {
        return factorialNum == 0 || factorialNum == 1 ? 1 :
            IntStream.rangeClosed(1, factorialNum).reduce(1, (a, b) -> a * b);
    }
}
