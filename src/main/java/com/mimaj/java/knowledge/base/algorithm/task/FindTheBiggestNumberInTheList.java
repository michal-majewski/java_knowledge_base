package com.mimaj.java.knowledge.base.algorithm.task;

import java.util.List;

class FindTheBiggestNumberInTheList {

    int recursive(List<Integer> integers) {
        return recursive(integers, 0, 0);
    }

    private int recursive(List<Integer> integers, int startingIndex, int currentBiggest) {
        if (integers.isEmpty()) {
            return 0;
        }

        if (integers.size() == 1) {
            return integers.get(0);
        }

        if (startingIndex == 0) {
            currentBiggest = integers.get(0);
        }

        if (startingIndex == integers.size()) {
            return currentBiggest;
        }

        if (integers.get(startingIndex) > currentBiggest) {
            currentBiggest = integers.get(startingIndex);
        }
        return recursive(integers, ++startingIndex, currentBiggest);
    }
}
