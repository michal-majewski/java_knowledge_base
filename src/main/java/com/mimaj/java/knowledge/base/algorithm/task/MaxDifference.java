package com.mimaj.java.knowledge.base.algorithm.task;

class MaxDifference {
    int find(final int[] givenNum) {
        int buyDayIndex = 0;
        int sellDayIndex = 0;
        int maxDiff = 0;

        for (int i = 0; i < givenNum.length; i++) {
            if(givenNum[i] < givenNum[buyDayIndex]) {
                buyDayIndex = i;
                sellDayIndex = i;
            }

            sellDayIndex = givenNum[sellDayIndex] > givenNum[i] ? sellDayIndex : i;

            var currentDiff = givenNum[sellDayIndex] - givenNum[buyDayIndex];
            maxDiff = Math.max(maxDiff, currentDiff);
        }

        return maxDiff == 0 ? -1 : maxDiff;
    }
}
