package com.mimaj.java.knowledge.base.algorithm.task;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

class MinutesBetweenStringHours {
    
    private static final String TIME_SEPARATOR = "-";
    private static final String HOUR_REGEX = "h:mm a";
    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern(HOUR_REGEX);
    private static final int WHOLE_DAY_MINUTES = 1440;

    long calc(final String timeExpression) {

        final var split = timeExpression.split(TIME_SEPARATOR);

        var earlyHour = split[0];
        var laterHour = split[1];
        
        earlyHour = fitToRegex(earlyHour);
        laterHour = fitToRegex(laterHour);

        final var earlyTime = LocalTime.parse(earlyHour, TIME_FORMATTER);
        final var laterTime = LocalTime.parse(laterHour, TIME_FORMATTER);

        final var timeDiff = ChronoUnit.MINUTES.between(earlyTime, laterTime);

        return isTimeDiffNegative(timeDiff) ? timeDiff + WHOLE_DAY_MINUTES : timeDiff;
    }

    private boolean isTimeDiffNegative(final long timeDiff) {
        return timeDiff < 0;
    }

    private String fitToRegex(final String stringHour) {
        final var digits = stringHour.substring(0, stringHour.length() - 2);
        final var dayPhase = stringHour.substring(stringHour.length() - 2);

        return digits + " " + dayPhase;
    }
}
