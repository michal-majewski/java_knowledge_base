package com.mimaj.java.knowledge.base.algorithm.task;

import java.util.*;

class NumberOccurrence {

    List<Integer> find(final int expectedOccurrence, final int[] givenArr) {
        final var numOccurrence = calcOccurrence(givenArr);

        return numOccurrence.entrySet().stream()
                .filter(e -> e.getValue() == expectedOccurrence)
                .map(Map.Entry::getKey)
                .sorted()
                .toList();
    }

    private HashMap<Integer, Integer> calcOccurrence(final int[] givenArr) {
        final var numOccurrence = new HashMap<Integer, Integer>();

        Arrays.stream(givenArr)
                .boxed()
                .forEach(i -> numOccurrence.merge(i, 1, (oldVal, newVal) -> oldVal + 1));

        return numOccurrence;
    }
}
