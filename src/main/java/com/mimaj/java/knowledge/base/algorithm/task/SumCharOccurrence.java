package com.mimaj.java.knowledge.base.algorithm.task;

class SumCharOccurrence {
    String asStringSum(final String given) {

        final var result = new StringBuilder();
        final String[] letters = given.split("");
        var counter = 0;
        var letter = letters[0];

        for (final String s : letters) {
            if(letter.equals(s)) {
                counter++;
            } else {
                result.append(counter).append(letter);
                letter = s;
                counter = 1;
            }
        }

        result.append(counter).append(letter);

        return result.toString();
    }
}
