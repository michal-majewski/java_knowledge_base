package com.mimaj.java.knowledge.base.algorithm.task;

import java.util.Arrays;

class SumOfIntArray {

    int recursive(int[] intArray) {
        if (intArray.length == 0) {
            return 0;
        }
        if (intArray.length == 1) {
            return intArray[0];
        }
        return intArray[0] + recursive(Arrays.copyOfRange(intArray, 1, intArray.length));
    }
}
