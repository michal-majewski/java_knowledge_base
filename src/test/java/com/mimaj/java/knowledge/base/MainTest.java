package com.mimaj.java.knowledge.base;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MainTest {

    @Test(groups = {"happy"})
    public void shouldAnswerWithTrue() {
        Assert.assertTrue(true, "It's should be a truth...");
    }

    @Test(groups = {"unhappy"})
    public void shouldAnswerWithFalse() {
        Assert.assertFalse(false, "It's should be a false...");
    }
}
