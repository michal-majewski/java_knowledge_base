package com.mimaj.java.knowledge.base.algorithm.groundhog.api;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertEquals;

public class AvgOfNumTest {

    @Test(dataProvider = "good-data-and-results")
    public void shouldReturnAverage_when_passedListOfNumbers(List<Integer> listOfNumbers, double expectedResult) {
        //when
        var actual = new AvgOfNum().streamSolution(listOfNumbers);
        //then
        assertEquals(actual, expectedResult);
    }

    @DataProvider(name="good-data-and-results")
    private Object[][] quickDataProvider() {
        return new Object[][] {
                {List.of(5), 5.0},
                {List.of(5, 4), 4.5},
                {List.of(5, 4, 6), 5.0},
                {List.of(5, 4, 6, 3), 4.5},
                {List.of(5,4,6,3,7,2), 4.5d}
        };
    }
}