package com.mimaj.java.knowledge.base.algorithm.groundhog.api;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class HexToDecTest {

    @Test(dataProvider = "good-data-and-result")
    public void shouldReturnProperString_when_providedGoodData(String input, String expected) {
        //when
        var actual = new HexToDec().convert(input);
        //then
        assertEquals(actual, expected);
    }

    @DataProvider(name="good-data-and-result")
    private Object[][] quickDataProvider() {
        return new Object[][] {
                {"0", "0"},
                {"F", "15"},
                {"10", "16"},
                {"20", "32"},
                {"B00B5", "721077"},
                {"CAFEBABE", "3405691582"},
                {"DEADBEEF", "3735928559"}
        };
    }
}