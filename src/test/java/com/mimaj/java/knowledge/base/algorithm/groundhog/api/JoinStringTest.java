package com.mimaj.java.knowledge.base.algorithm.groundhog.api;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertEquals;

public class JoinStringTest {

    @Test(dataProvider = "good-data-and-results")
    public void shouldReturnProperlyJoinedString_when_passedTheProperData(List<String> strings, String expected) {
        //when
        var actual = new JoinString().joinAllUsingStringApi(strings);
        //then
        assertEquals(actual, expected);
    }

    @DataProvider(name="good-data-and-results")
    private Object[][] quickDataProvider() {
        return new Object[][] {
                {List.of("a"), "a"},
                {List.of("a", "b"), "ab"},
                {List.of("a", "b", "c"), "abc"},
                {List.of("a", "b", "c", "d"), "abcd"},
                {List.of("a","b","c","d","e","f"), "abcdef"},
                {List.of("a","b","c","d","e","f","g","h"), "abcdefgh"}
        };
    }
}