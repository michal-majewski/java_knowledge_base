package com.mimaj.java.knowledge.base.algorithm.groundhog.api;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class MaxNumberTest {

    @Test(dataProvider = "good-data")
    public void shouldReturnLargestNumber_when_provideProperData(int[] numbers, int expected) {
        //when
        var actual = new MaxNumber().findUsingStreams(numbers);
        //then
        assertEquals(actual, expected);
    }

    @DataProvider(name="good-data")
    private Object[][] quickDataProvider() {
        return new Object[][] {
                {new int[]{5}, 5},
                {new int[]{5, 5}, 5},
                {new int[]{5, 4}, 5},
                {new int[]{5,4,6,3,7,2}, 7}
        };
    }
}