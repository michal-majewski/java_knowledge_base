package com.mimaj.java.knowledge.base.algorithm.groundhog.api;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class ReverseStringTest {

    @Test(dataProvider = "good-data")
    public void shouldReturnReversedString_when_providedProperString(String given, String expected) {
        //when
        var actual = new ReverseString().perform(given);
        //then
        assertEquals(actual, expected);
    }

    @DataProvider(name="good-data")
    private Object[][] quickDataProvider() {
        return new Object[][] {
                {"q", "q"},
                {"qu", "uq"},
                {"qua", "auq"},
                {"quac", "cauq"},
                {"quack", "kcauq"},
                {"aibohphobia", "aibohphobia"}
        };
    }
}