package com.mimaj.java.knowledge.base.algorithm.groundhog.api;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class SumOfNumbersTest {

    @Test(dataProvider = "good-data")
    public void shouldReturnSumOfNumber_when_givenArrayWithNumbers(int[] numbers, int expected) {
        //when
        var actual = new SumOfNumbers().find(numbers);
        //then
        assertEquals(actual, expected);
    }

    @DataProvider(name = "good-data")
    private Object[][] quickDataProvider() {
        return new Object[][]{
                {new int[]{}, 0},
                {new int[]{5}, 5},
                {new int[]{5,4}, 9},
                {new int[]{5,4,6}, 15},
                {new int[]{5,4,6,3}, 18},
                {new int[]{5,4,6,3,7,2}, 27}
        };
    }
}