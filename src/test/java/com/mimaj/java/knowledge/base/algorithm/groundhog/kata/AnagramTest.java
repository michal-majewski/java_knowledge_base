package com.mimaj.java.knowledge.base.algorithm.groundhog.kata;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class AnagramTest {

    @Test(dataProvider = "anagram")
    public void shouldReturnTrue_when_passedAnagrams(String firstWord, String secondWord) {
        //given & when
        var actual = new Anagram().check(firstWord, secondWord);
        //then
        assertTrue(actual);
    }

    @Test(dataProvider = "not-anagram")
    public void shouldReturnFalse_when_passedNotAnagrams(String firstWord, String secondWord) {
        //given & when
        var actual = new Anagram().check(firstWord, secondWord);
        //then
        assertFalse(actual);
    }

    @DataProvider(name="anagram")
    private Object[][] properAnagramProvider() {
        return new Object[][] {
                {"",""},
                {"ELVIS","LIVES"},
                {"FUNERAL","REALFUN"},
                {"ELEVEN PLUS TWO","TWELVE PLUS ONE"},
                {"TOMMARVOLORIDDLE", "IAMLORDVOLDEMORT"}
        };
    }

    @DataProvider(name="not-anagram")
    private Object[][] improperAnagramProvider() {
        return new Object[][] {
                {"1",""},
                {"","1"},
                {"a","aa"},
                {"bb","b"},
                {"abc","abb"},
                {"ELEVEN PLUS TWO","TWELVE PLUS ONEE"},
                {"ELEVEN PLUS TWWO","TWELVE PLUS ONE"}
        };
    }
}