package com.mimaj.java.knowledge.base.algorithm.groundhog.kata;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class Count3Test {

    @Test(dataProvider = "good-data")
    public void shouldReturnCorrectValueOfThree_when_passedProperInput(String number, int expected) {
        //when
        var actual = new Count3().streamCheck(number);
        //then
        assertEquals(actual, expected);
    }

    @DataProvider(name = "good-data")
    private Object[][] quickDataProvider() {
        return new Object[][]{
                {"0", 0},
                {"3", 1},
                {"00", 0},
                {"30", 1},
                {"33", 2},
                {"030", 1},
                {"303", 2},
                {"3030303", 4}
        };
    }
}