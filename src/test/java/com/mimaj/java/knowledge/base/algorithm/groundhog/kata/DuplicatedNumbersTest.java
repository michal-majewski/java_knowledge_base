package com.mimaj.java.knowledge.base.algorithm.groundhog.kata;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class DuplicatedNumbersTest {

    @Test(dataProvider = "data")
    public void shouldCountDuplicationOccurrence_when_passedArrayWith(String[] numberArr, int expected) {
        //given & when
        var actual = new DuplicatedNumbers().count(numberArr);
        //then
        assertEquals(actual, expected);
    }

    @DataProvider(name= "data")
    private Object[][] quickDataProvider() {
        return new Object[][] {
                {new String[0], 0},
                {new String[]{"1"}, 0},
                {new String[]{"1", "2"}, 0},
                {new String[]{"1", "1"}, 1},
                {new String[]{"5","4","6","3","5","2"}, 5}
        };
    }
}