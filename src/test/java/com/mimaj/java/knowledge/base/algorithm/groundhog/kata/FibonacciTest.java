package com.mimaj.java.knowledge.base.algorithm.groundhog.kata;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class FibonacciTest {

    @Test(dataProvider = "data")
    public void shouldCalcProperFiboNumber_when_usingBasicRecursive(int fiboNum, long expected) {
        //given & when
        var actual = new Fibonacci().calcBasicRecursive(fiboNum);
        //then
        assertEquals(actual, expected);
    }

    @Test(dataProvider = "data")
    public void shouldCalcProperFiboNumber_when_usingEnhancedRecursive(int fiboNum, long expected) {
        //given & when
        var actual = new Fibonacci().calcEnhancedRecursive(fiboNum);
        //then
        assertEquals(actual, expected);
    }

    @DataProvider(name="data")
    private Object[][] quickDataProvider() {
        return new Object[][] {
                {0, 0},
                {1, 1},
                {2, 1},
                {3, 2},
                {7, 13},
                {46, 1836311903},
                {47, 2971215073L}
        };
    }
}