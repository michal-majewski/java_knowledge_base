package com.mimaj.java.knowledge.base.algorithm.groundhog.kata;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class Find3Test {

    @Test(dataProvider = "good-data")
    public void shouldReturnIndexOfDigitThree_when_passedProperArray(int[] numbers, int expected) {
        //when
        var actual = new Find3().indexOfFirstEncountered(numbers);
        //then
        assertEquals(actual, expected);
    }

    @DataProvider(name="good-data")
    private Object[][] quickDataProvider() {
        return new Object[][] {
                {new int[0], -1},
                {new int[]{5}, -1},
                {new int[]{3}, 0},
                {new int[]{3, 5}, 0},
                {new int[]{5, 3}, 1},
                {new int[]{5,4,6,3,7,2}, 3}
        };
    }
}