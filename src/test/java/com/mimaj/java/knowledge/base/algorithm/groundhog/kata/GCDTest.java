package com.mimaj.java.knowledge.base.algorithm.groundhog.kata;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class GCDTest {

    @Test(dataProvider = "good-data")
    public void shouldReturnGreatestCommonDivisor_when_passedProperValues(int first, int second, int expected) {
        //given & when
        var actual = new GCD().find(first, second);
        //then
        assertEquals(actual, expected);
    }

    @DataProvider(name = "good-data")
    private Object[][] quickDataProvider() {
        return new Object[][]{
                {1, 1, 1},
                {1, 2, 1},
                {2, 3, 1},
                {2, 4, 2},
                {12, 8, 4},
                {282, 78, 6},
                {78, 282, 6},
                {28, 4628, 4},
                {4628, 28, 4},
                {1836311903, 1134903170, 1},
                {1134903170, 1836311903, 1}
        };
    }
}