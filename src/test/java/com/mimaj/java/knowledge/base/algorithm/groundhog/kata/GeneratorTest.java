package com.mimaj.java.knowledge.base.algorithm.groundhog.kata;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class GeneratorTest {

    @Test(dataProvider = "good-data")
    public void shouldReturnAllGenerators_when_passedProperValue(int primeNUmber, int[] expected) {
        //given & when
        var actual = new Generator().findGenerators(primeNUmber);
        //then
        assertEquals(actual, expected);
    }

    @DataProvider(name = "good-data")
    private Object[][] quickDataProvider() {
        return new Object[][]{
                {2, new int[]{1}},
                {3, new int[]{2}},
                {5, new int[]{2, 3}},
                {7, new int[]{3, 5}},
                {11, new int[]{2, 6, 7, 8}},
                {13, new int[]{2, 6, 7, 11}},
                //FIXME: Check task assumptions
//                {17, new int[]{3, 5, 6, 7, 10, 11, 12, 14}},
//                {23, new int[]{5, 7, 10, 11, 14, 15, 17, 19, 20, 21}},
        };
    }
}