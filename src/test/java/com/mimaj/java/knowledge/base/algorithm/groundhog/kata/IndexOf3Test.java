package com.mimaj.java.knowledge.base.algorithm.groundhog.kata;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class IndexOf3Test {

    @Test(dataProvider = "good-data")
    public void shouldFindIndexOf3_when_passedProperData(int[] numbers, int expected) {
        //given & when
        var actual = new IndexOf3().find(numbers);
        //then
        Assert.assertEquals(actual, expected);
    }

    @DataProvider(name = "good-data")
    private Object[][] quickDataProvider() {
        return new Object[][]{
                {new int[0], -1},
                {new int[]{5}, -1},
                {new int[]{3}, 0},
                {new int[]{3, 5}, 0},
                {new int[]{5, 3}, 1},
                {new int[]{5, 4, 6, 3, 7, 2}, 3}
        };
    }
}