package com.mimaj.java.knowledge.base.algorithm.groundhog.kata;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class MedianTest {

    @Test(dataProvider = "good-data")
    public void shouldCalculateMedian_when_passedProperData(int[] numbers, float expected) {
        //given & when
        var actual = new Median().calc(numbers);
        //then
        assertEquals(actual, expected);
    }

    @Test(dataProvider = "good-data")
    public void shouldCalculateMedianInRecursiveWay_when_passedProperData(int[] numbers, float expected) {
        //given & when
        var actual = new Median().calcRecursive(numbers);
        //then
        assertEquals(actual, expected);
    }


    @DataProvider(name = "good-data")
    private Object[][] quickDataProvider() {
        return new Object[][]{
                {new int[]{6, 4, 2, 4, 4}, 4.0f},
                {new int[]{5,8,-1,6,6,1,10}, 6.0f},
                {new int[]{7,8,3,4,9,2}, 5.5f},
                {new int[]{5, 4, 6, 3, 1, 2}, 3.5f}
        };
    }
}