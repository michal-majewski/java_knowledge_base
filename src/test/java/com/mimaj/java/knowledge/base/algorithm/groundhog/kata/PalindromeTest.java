package com.mimaj.java.knowledge.base.algorithm.groundhog.kata;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class PalindromeTest {

    @Test(dataProvider = "true-palindromes")
    public void shouldRecognizePalindrome_when_passedPalindromes(String word) {
        //when
        var actual = new Palindrome().check(word);
        //then
        assertTrue(actual);
    }

    @Test(dataProvider = "false-palindromes")
    public void shouldRecognizeNonPalindromeWords_when_passedNonPalindromes(String word) {
        //when
        var actual = new Palindrome().check(word);
        //then
        assertFalse(actual);
    }

    @DataProvider(name="true-palindromes")
    private Object[][] truePalindromes() {
        return new Object[][] {
                {"i"},
                {"ii"},
                {"mim"},
                {"madamimadam"},
                {"aibohphobia"}
        };
    }

    @DataProvider(name="false-palindromes")
    private Object[][] falsePalindromes() {
        return new Object[][] {
                {"coffee"},
                {"mi"},
                {"im"}
        };
    }
}
