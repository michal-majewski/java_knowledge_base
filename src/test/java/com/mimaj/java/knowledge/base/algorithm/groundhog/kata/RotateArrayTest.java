package com.mimaj.java.knowledge.base.algorithm.groundhog.kata;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class RotateArrayTest {

    @Test(dataProvider = "good-data")
    public void shouldRotateArrayOneLeft_when_passedProperData(int[] givenArray, int[] expected) {
        //when
        var actual = new RotateArray().oneLeft(givenArray);
        //then
        assertEquals(actual, expected);
    }

    @DataProvider(name="good-data")
    private Object[][] quickDataProvider() {
        return new Object[][] {
                {new int[]{5}, new int[]{5}},
                {new int[]{5,4}, new int[]{4,5}},
                {new int[]{5,4,6}, new int[]{4,6,5}},
                {new int[]{1,2,3,4,5,6}, new int[]{2,3,4,5,6,1}},
        };
    }
}