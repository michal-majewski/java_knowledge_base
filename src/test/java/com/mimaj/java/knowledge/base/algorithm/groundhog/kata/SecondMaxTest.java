package com.mimaj.java.knowledge.base.algorithm.groundhog.kata;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class SecondMaxTest {

    @Test(dataProvider = "good-data")
    public void shouldReturnSecondMaxNumber_when_passedProperData(int[] numbers, int expected) {
        //given & when
        var actual = new SecondMax().find(numbers);
        //then
        assertEquals(actual, expected);
    }

    @DataProvider(name = "good-data")
    private Object[][] quickDataProvider() {
        return new Object[][]{
                {new int[]{5}, 0},
                {new int[]{5, 5}, 0},
                {new int[]{5, 4}, 4},
                {new int[]{5, 4, 6, 3, 7, 2}, 6},
                {new int[]{472, 135, 834, 714, 534}, 714}
        };
    }
}