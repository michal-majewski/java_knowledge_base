package com.mimaj.java.knowledge.base.algorithm.groundhog.kata;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class SumOfEvenNumbersTest {

    @Test(dataProvider = "good-data")
    public void shouldReturnCorrectSumOfEvenNumbers_when_passedProperArray(int[] numbers, int expected) {
        //when
        var actual = new SumOfEvenNumbers().calc(numbers);
        //then
        assertEquals(actual, expected);
    }

    @DataProvider(name="good-data")
    private Object[][] quickDataProvider() {
        return new Object[][] {
                {new int[0], 0},
                {new int[]{5}, 0},
                {new int[]{5,4}, 4},
                {new int[]{5,4,6}, 10},
                {new int[]{5,4,6,3}, 10},
                {new int[]{5,4,6,3,7,2}, 12}
        };
    }
}