package com.mimaj.java.knowledge.base.algorithm.regex;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CountNumbersTest {

    @Test(dataProvider = "text-with-numbers")
    public void shouldCountAllNumbers_when_passedTextWithNumbers(String txt, int expected) {
        //given & when
        var actual = new CountNumbers().countInString(txt);
        //then
        assertEquals(actual, expected);
    }

    @Test(dataProvider = "text-without-numbers")
    public void shouldReturn0_when_passedTextWithoutNumbers(String txt) {
        //given
        var expected = 0;
        //when
        var actual = new CountNumbers().countInString(txt);
        //then
        assertEquals(actual, expected);
    }

    @DataProvider(name = "text-with-numbers")
    private Object[][] textWithNumberProvider() {
        return new Object[][]{
                {"Why did the Star Wars episodes 4, 5 and 6 come before 1, 2 and 3? Because in charge of planning, Yoda was.", 6},
                {"22:36:44", 3},
                {"99 percent of all statistics only tell 49 percent of the story.", 2}
        };
    }

    @DataProvider(name = "text-without-numbers")
    private Object[][] textWithoutNumberProvider() {
        return new Object[][]{
                {""},
                {"String without numbers"}
        };
    }
}