package com.mimaj.java.knowledge.base.algorithm.regex;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class TimeValidationTest {

    @Test(dataProvider = "valid-format")
    public void shouldReturnTrue_when_passedValidTime(String time) {
        //when
        var actual = new TimeValidation().isValid(time);
        //then
        assertTrue(actual);
    }

    @Test(dataProvider = "invalid-format")
    public void shouldReturnFalse_when_passedValidTime(String time) {
        //when
        var actual = new TimeValidation().isValid(time);
        //then
        assertFalse(actual);
    }

    @DataProvider(name = "valid-format")
    private Object[][] validTimeProvider() {
        return new Object[][]{
                {"00:00:00"},
                {"22:36:44"},
                {"22:36:44"},
                {"23:59:59"}
        };
    }

    @DataProvider(name = "invalid-format")
    private Object[][] invalidTimeProvider() {
        return new Object[][]{
                {"24:33:44"},
                {"09:61:44"},
                {"01:21:84"}
        };
    }
}