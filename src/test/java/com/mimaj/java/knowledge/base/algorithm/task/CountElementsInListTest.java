package com.mimaj.java.knowledge.base.algorithm.task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CountElementsInListTest {

    @Test(groups = {"happy"}, dataProvider = "correct-values-and-results")
    public void shouldReturnCorrectResult_When_UsingRecursiveMethod(List<Integer> providedList,
                                                                    int expected) {
        //given & when
        var actual = new CountElementsInList().recursive(providedList);
        //then
        Assert.assertEquals(actual, expected, "The actual value is not the same as the expected");
    }

    @DataProvider(name = "correct-values-and-results")
    private Object[][] provideCorrectValuesWithCorrect() {
        return new Object[][] {
            {new ArrayList<>(Collections.emptyList()), 0},
            {new ArrayList<>(List.of(1)), 1},
            {new ArrayList<>(List.of(1, 2)), 2},
            {new ArrayList<>(List.of(1, 2, 3, 4, 5)), 5},
            {new ArrayList<>(IntStream.rangeClosed(1, 100).boxed().toList()), 100},
        };
    }
}