package com.mimaj.java.knowledge.base.algorithm.task;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class FactorialTest {

    @Test(groups = {"happy"}, dataProvider = "correct-values-and-results")
    public void shouldReturnCorrectResult_When_UsingRecursiveMethod(int factorialNum,
                                                                    int expected) {
        //given & when
        var actual = new Factorial().recursive(factorialNum);
        //then
        Assert.assertEquals(actual, expected, "The actual value is not the same as the target");
    }

    @Test(groups = {"happy"}, dataProvider = "correct-values-and-results")
    public void shouldReturnCorrectResult_When_UsingImperativeMethod(int factorialNum,
                                                                     int expected) {
        //given & when
        var actual = new Factorial().imperative(factorialNum);
        //then
        Assert.assertEquals(actual, expected, "The actual value is not the same as the target");
    }

    @Test(groups = {"happy"}, dataProvider = "correct-values-and-results")
    public void shouldReturnCorrectResult_When_UsingStreamsMethod(int factorialNum, int expected) {
        //given & when
        var actual = new Factorial().stream(factorialNum);
        //then
        Assert.assertEquals(actual, expected, "The actual value is not the same as the target");
    }

    @DataProvider(name = "correct-values-and-results")
    private Object[][] provideCorrectValuesWithCorrect() {
        return new Object[][] {
            {0, 1},
            {1, 1},
            {3, 6},
            {5, 120},
            {10, 3628800},
            {12, 479001600}
        };
    }
}