package com.mimaj.java.knowledge.base.algorithm.task;

import java.util.Collections;
import java.util.List;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class FindTheBiggestNumberInTheListTest {

    @Test(dataProvider = "valid-list")
    public void shouldReturnTheBiggestNumberInTheList_When_ProvidedValidList(List<Integer> numbers,
                                                                             int expected) {
        //given & when
        var actual = new FindTheBiggestNumberInTheList().recursive(numbers);
        //then
        Assert.assertEquals(actual, expected);
    }

    @DataProvider(name = "valid-list")
    private Object[][] quickDataProvider() {
        return new Object[][] {
            {Collections.emptyList(), 0},
            {List.of(5), 5},
            {List.of(1, 2, 3, 4, 5), 5},
            {List.of(6, 6, 6, 6, 6), 6},
            {List.of(0, 0, 0, 0, 0), 0},
            {List.of(Integer.MIN_VALUE, -44_555, -34, -10, -50), -10},
            {List.of(-10_000, 50, 10_001), 10_001},
            {List.of(Integer.MIN_VALUE, 0, Integer.MAX_VALUE), Integer.MAX_VALUE}
        };
    }
}