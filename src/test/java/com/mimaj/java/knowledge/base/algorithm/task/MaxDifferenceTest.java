package com.mimaj.java.knowledge.base.algorithm.task;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class MaxDifferenceTest {

    @Test(dataProvider = "correct-data")
    public void shouldFindMaxDifference(int[] givenNum, int expected) {
        //given & when
        var actual = new MaxDifference().find(givenNum);
        //then
        assertEquals(actual, expected);
    }

    @DataProvider(name="correct-data")
    private Object[][] correctDataProvider() {
        return new Object[][] {
                {new int[]{1,2,3,4,5}, 4},
                {new int[]{10,7,4,2,1}, -1},
                {new int[]{0,0,0,0,0}, -1},
                {new int[]{1,7,5,6,10}, 9},
                {new int[]{1,7,5,6,7}, 6},
                {new int[]{3,10,2,5,10}, 8},
                {new int[]{3,9,2,5,10}, 8}
        };
    }
}