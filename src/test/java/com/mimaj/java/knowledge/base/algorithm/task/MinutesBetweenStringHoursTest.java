package com.mimaj.java.knowledge.base.algorithm.task;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class MinutesBetweenStringHoursTest {

    @Test(dataProvider = "correct-data")
    public void shouldReturnProperMinutesDiff(String timeExpression, long expected) {
        //given & when
        var actual = new MinutesBetweenStringHours().calc(timeExpression);
        //then
        assertEquals(actual, expected);
    }

    @DataProvider(name="correct-data")
    private Object[][] correctDataProvider() {
        return new Object[][] {
                {"6:55PM-12:00AM", 305},
                {"12:00AM-11:59PM", 1439},
                {"12:00AM-12:00AM", 0},
                {"12:30PM-11:30AM", 1380},
                {"12:00AM-12:30PM", 750},
                {"11:30AM-12:14PM", 44}
        };
    }
}