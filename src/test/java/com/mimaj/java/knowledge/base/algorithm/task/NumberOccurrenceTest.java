package com.mimaj.java.knowledge.base.algorithm.task;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertEquals;

public class NumberOccurrenceTest {

    @Test(dataProvider = "correct-data")
    public void shouldFindCorrectNumbers(int expectedOccurrence, int[] givenArr, List<Integer> expected) {
        //given & when
        var actual = new NumberOccurrence().find(expectedOccurrence, givenArr);
        //then
        assertEquals(actual, expected);
    }

    @DataProvider(name="correct-data")
    private Object[][] correctDataProvider() {
        return new Object[][] {
                {2, new int[]{-5, -5,-2,-1,-1,0,1,1,1,2,2,3}, List.of(-5,-1,2)},
                {1, new int[]{-4,-3,-3,0,1,1,1,2,3,5,5,5}, List.of(-4, 0, 2, 3)}
        };
    }
}
