package com.mimaj.java.knowledge.base.algorithm.task;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class SumCharOccurrenceTest {

    @Test(dataProvider ="correct-data")
    public void quickTrueTest(String given, String expected) {
        //given & when
        var actual = new SumCharOccurrence().asStringSum(given);
        //then
        assertEquals(actual, expected);
    }

    @DataProvider(name="correct-data")
    private Object[][] correctDataProvider() {
        return new Object[][] {
                {"abc", "1a1b1c"},
                {"aabcaa", "2a1b1c2a"},
                {"wwwabc", "3w1a1b1c"}
        };
    }
}