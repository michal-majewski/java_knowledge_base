package com.mimaj.java.knowledge.base.algorithm.task;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SumOfIntArrayTest {

    @Test(groups = {"happy"}, dataProvider = "correct-values-and-results")
    public void shouldReturnCorrectResult_When_UsingRecursiveMethod(int[] providedArray,
                                                                    int expected) {
        //given & when
        var actual = new SumOfIntArray().recursive(providedArray);
        //then
        Assert.assertEquals(actual, expected, "The actual value is not the same as the expected");
    }

    @DataProvider(name = "correct-values-and-results")
    private Object[][] provideCorrectValuesWithCorrect() {
        return new Object[][] {
            {new int[] {}, 0},
            {new int[] {25}, 25},
            {new int[] {11, 9}, 20},
            {new int[] {1, 99, 45, 65}, 210},
            {new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, 55},
        };
    }
}